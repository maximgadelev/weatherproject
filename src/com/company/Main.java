package com.company;

import java.io.*;
import java.util.ArrayList;
public class Main {

    public static void main(String[] args) {
        writeFile(findInformationAboutWeather(readFile("dataexport_20210320T064822.csv")));
    }

    public static ArrayList<String> readFile(String path) {
        try {
            ArrayList<String> allStringsInTheFile = new ArrayList<>();
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                allStringsInTheFile.add(line);
            }
            bufferedReader.close();
            return allStringsInTheFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList <String> findInformationAboutWeather(ArrayList<String> allStringInTheFile) {
        //
        double maximumTemperature=Double.MIN_VALUE;
        String dayAndHourWithHighestTemp="";
        double minHumidity=Double.MAX_VALUE;
        double maxWindSpeed=Double.MIN_VALUE;
        String dayAndHourLowestHimidit="";
        String dayAndHourFastWind="";
        //
        double averageTemperature=0;
        double averageWind=0;
        double averageHumidite=0;
        //
        int [] countOfWideDirection=new int[8];
        String frequentDirection="";
        for (int i = 10; i < allStringInTheFile.size(); i++) {
            String[] stringsWithInformation = allStringInTheFile.get(i).split(",");
            String date=stringsWithInformation[0];
            double windDirection=Double.parseDouble(stringsWithInformation[4]);
            double wind = Double.parseDouble(stringsWithInformation[3]);
            double humidity = Double.parseDouble(stringsWithInformation[2]);
            double temperature=Double.parseDouble(stringsWithInformation[1]);
            if(temperature>maximumTemperature){
                maximumTemperature=temperature;
                dayAndHourWithHighestTemp=date;
            }
            if(humidity<minHumidity){
                minHumidity=humidity;
                dayAndHourLowestHimidit=date;
            }
            if(wind>maxWindSpeed){
                maxWindSpeed=wind;
                dayAndHourFastWind=date;
            }
            averageHumidite=averageHumidite+humidity;
            averageTemperature=averageTemperature+temperature;
            averageWind=averageWind+wind;
            if(windDirection<22.5 && windDirection>=337.5){
                countOfWideDirection[0]++;
            }else if(windDirection>=22.5 && windDirection<67.5){
                countOfWideDirection[1]++;
            }else if(windDirection>=67.5 && windDirection<112.5){
                countOfWideDirection[2]++;
            }else if(windDirection>=112.5&& windDirection<157.5){
                countOfWideDirection[3]++;
            }else if(windDirection>=157.5 && windDirection<202.5){
                countOfWideDirection[4]++;
            }else if(windDirection>=202.5 && windDirection<247.5){
                countOfWideDirection[5]++;
            }else if(windDirection>=247.5 && windDirection<292.5){
                countOfWideDirection[6]++;
            }else if(windDirection>=292.5 && windDirection<337.5){
                countOfWideDirection[7]++;
            }
        }
        int maximumCount=-1;
        int maximumCountIndex=0;
        for (int i = 0; i <8 ; i++) {
            if (countOfWideDirection[i]>maximumCount){
                maximumCountIndex=i;
                maximumCount=countOfWideDirection[i];
            }
        }
        switch (maximumCountIndex) {
            //джава предложила сократить строки кейсов,почему бы и нет
            case (0) -> frequentDirection = "Север";
            case (1) -> frequentDirection = "Северо-Восток";
            case (2) -> frequentDirection = "Восток";
            case (3) -> frequentDirection = "Юго-Восток";
            case (4) -> frequentDirection = "Юг";
            case (5) -> frequentDirection = "Юго-Запад";
            case (6) -> frequentDirection = "Запад";
            case (7) -> frequentDirection = "Северо-Запад";
        }
        averageHumidite=averageHumidite/(allStringInTheFile.size()-10);
        averageTemperature=averageTemperature/(allStringInTheFile.size()-10);
        averageWind=averageWind/(allStringInTheFile.size()-10);
        ArrayList <String> information= new ArrayList<>();
        information.add("Средняя температура " + averageTemperature +" °C");
        information.add("Средняя влажность " + averageHumidite +" %");
        information.add("Средняя скорость ветра " +averageWind+" км.ч");
        information.add("Cамая высокая температура "+maximumTemperature + "°C " + doValidDate(dayAndHourWithHighestTemp).get(0) + " "+ doValidDate(dayAndHourWithHighestTemp).get(1));
        information.add("Самая низкая влажность " + minHumidity + " % " + doValidDate(dayAndHourLowestHimidit).get(0)+ " "+ doValidDate(dayAndHourLowestHimidit).get(1));
        information.add("Cамый сильный ветер " + maxWindSpeed +" км.ч " + doValidDate(dayAndHourFastWind).get(0) + " " + doValidDate(dayAndHourFastWind).get(1));
        information.add("Самая частоe направление ветра " + frequentDirection);
        return information;
    }
    public static void writeFile(ArrayList<String> currentInformation){
        try{
            File file = new File("C:\\Users\\111\\untitled2","Information.txt");
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for(String information:currentInformation){
                bufferedWriter.write(information +"\n");
            }
            bufferedWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static ArrayList<String> doValidDate(String dayAndHour){
        String [] s=dayAndHour.split("T");
        StringBuilder data = new StringBuilder(s[0]);
        data.insert(4,".");
        data.insert(7,".");
        StringBuilder time = new StringBuilder(s[1]);
        time.insert(2,":");
        ArrayList<String> validDateAndHour=new ArrayList<>();
        validDateAndHour.add(data.toString());
        validDateAndHour.add(time.toString());
        return  validDateAndHour;
    }

}